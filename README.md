# Go lang

## About

Collection of links to documents, tutorials etc. about [Go lang](https://golang.org)

## Tutorials

* [Learn Go with tests](https://github.com/quii/learn-go-with-tests)

## Go way

* https://golang.org
  * [Effective Go](https://golang.org/doc/effective_go.html)
* https://github.com/golang/go
  * [Table Driven Tests](https://github.com/golang/go/wiki/TableDrivenTests)
